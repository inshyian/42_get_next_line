/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printmem.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/03 15:53:36 by ishyian           #+#    #+#             */
/*   Updated: 2018/12/03 16:03:35 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "libft.h"

void		ft_printmem(void const *mem, size_t size)
{
	size_t	i;

	i = -1;
	while (++i < size)
		ft_putchar(((unsigned char*)mem)[i]);
}
